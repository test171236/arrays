#include <iostream>

#include <time.h>  

int main()
{
    const size_t N = 10;

    int arr[N][N];
    for (size_t i = 0; i < N; i++)
        for (size_t j = 0; j < N; j++)
            arr[i][j] = i + j;

    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < N; j++)
            std::cout << arr[i][j] << (j == N - 1 ? "" : ", ");
        std::cout << std::endl;
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int sum = 0;
    for (size_t i = 0; i < N; i++)
        sum += arr[buf.tm_mday % N][i];

    std::cout << sum;

    return 0;
}